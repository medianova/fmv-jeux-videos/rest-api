# ####################### #
# ##### Application ##### #
# ####################### #
PROJECT_NAME=fmv

app.init:
	composer install --no-dev --optimize-autoloader -n
	APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
	./scripts/database.sh

dev:
	composer install
	APP_ENV=dev APP_DEBUG=1 php bin/console cache:clear
