<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220420125135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie ADD uploader_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE movie ADD CONSTRAINT FK_1D5EF26F16678C77 FOREIGN KEY (uploader_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_1D5EF26F16678C77 ON movie (uploader_id)');
        $this->addSql('ALTER TABLE video CHANGE choices choices LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', CHANGE child_id child_id LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie DROP FOREIGN KEY FK_1D5EF26F16678C77');
        $this->addSql('DROP INDEX IDX_1D5EF26F16678C77 ON movie');
        $this->addSql('ALTER TABLE movie DROP uploader_id');
        $this->addSql('ALTER TABLE video CHANGE choices choices LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', CHANGE child_id child_id LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
    }
}
