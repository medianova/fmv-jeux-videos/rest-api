Api FMV Jeux vidéoss
========================

Notre projet FMV Jeux vidéos est développé suite à la création de notre projet de plateforme de films interactifs dans le cadre des Ydays au sein d'Ynov Lyon.

Requirements
------------

* PHP 8.0.2 or higher;
* PDO-SQLite PHP extension enabled;
* and the [usual Symfony application requirements][2].

Installation
------------

[Download Symfony][4] to install the `symfony` binary on your computer.

Usage
-----

There's no need to configure anything to run the application. If you have
[installed Symfony][4] binary, run this command:

```bash
$ cd rest-api/
$ php bin/console doctrine:database:create
$ php bin/console doctrine:migrations:migrate
$ symfony server:start --no-tls
```

Then access the application in your browser at the given URL (<https://localhost:8000> by default).

If you don't have the Symfony binary installed, run `php -S localhost:8000 -t public/`
to use the built-in PHP web server or [configure a web server][3] like Nginx or
Apache to run the application.

[2]: https://symfony.com/doc/current/setup.html#technical-requirements
[3]: https://symfony.com/doc/current/setup/web_server_configuration.html
[4]: https://symfony.com/download
