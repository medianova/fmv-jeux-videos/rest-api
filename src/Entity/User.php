<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ApiResource(
 *     normalizationContext={"groups"={"user:read"}},
 *     denormalizationContext={"groups"={"user:write"}},
 *     collectionOperations={
 *         "get",
 *         "post",
 *         "login" = {
 *             "security" = "is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *             "route_name" = "authentication_token",
 *             "method" = "POST",
 *             "openapi_context" = {
 *                 "summary" = "Login method",
 *                 "requestBody" = {
 *                     "description" = "Get token",
 *                     "content" = {
 *                         "application/json" = {
 *                             "schema" = {
 *                                 "type" = "object",
 *                                 "required" = {
 *                                     "username",
 *                                     "password"
 *                                 },
 *                                 "properties" = {
 *                                     "username" = {
 *                                         "type" = "string"
 *                                     },
 *                                     "password" = {
 *                                         "type" = "string"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *     "register" = {
 *             "security" = "is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *             "path"="/register",
 *             "method" = "POST",
 *             "openapi_context" = {
 *                 "summary" = "Register method",
 *                 "requestBody" = {
 *                     "description" = "Register a user",
 *                     "content" = {
 *                         "application/json" = {
 *                             "schema" = {
 *                                 "type" = "object",
 *                                 "required" = {
 *                                     "email",
 *                                     "password",
 *                                     "username"
 *                                 },
 *                                 "properties" = {
 *                                     "email" = {
 *                                         "type" = "string"
 *                                     },
 *                                     "password" = {
 *                                         "type" = "string"
 *                                     },
 *                                     "username" = {
 *                                         "type" = "string"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @method string getUserIdentifier()
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read", "movie:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups({"user:read", "user:write", "movie:read", "movie:write"})
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @Groups({"user:write"})
     * @SerializedName("password")
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups({"user:read", "user:write", "movie:read", "movie:write"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "user:write"})
     */
    private $avatar;

    /**
     * @ORM\ManyToMany(targetEntity=Movie::class, mappedBy="likes")
     * @Groups({"user:read", "user:write"})
     */
    private $likes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "user:write"})
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity=Movie::class, mappedBy="uploader")
     * @Groups({"user:read","user:write"})
     */
    private $movies;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
        $this->movies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Movie $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->addLike($this);
        }

        return $this;
    }

    public function removeLike(Movie $like): self
    {
        if ($this->likes->removeElement($like)) {
            $like->removeLike($this);
        }

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    /**
     * @return Collection|Movie[]
     */
    public function getMovies(): Collection
    {
        return $this->movies;
    }

    public function addMovie(Movie $movie): self
    {
        if (!$this->movies->contains($movie)) {
            $this->movies[] = $movie;
            $movie->setUploader($this);
        }

        return $this;
    }

    public function removeMovie(Movie $movie): self
    {
        if ($this->movies->removeElement($movie)) {
            // set the owning side to null (unless already changed)
            if ($movie->getUploader() === $this) {
                $movie->setUploader(null);
            }
        }

        return $this;
    }
}
