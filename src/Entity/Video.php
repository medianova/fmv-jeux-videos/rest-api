<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\VideoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"video:read"}},
 *     denormalizationContext={"groups"={"video:write"}}
 * )
 * @ORM\Entity(repositoryClass=VideoRepository::class)
 */
class Video
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"video:read","movie:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $question;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $choices = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $parentId = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $childId = [];

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity=Movie::class, inversedBy="videos",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups("video:write")
     */
    private $movie;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"video:read", "video:write","movie:read"})
     */
    private $parent = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(?string $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return array
     */
    public function getParentId(): array
    {
        return $this->parentId;
    }

    /**
     * @param array $parentId
     */
    public function setParentId(array $parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @return array
     */
    public function getChildId(): array
    {
        return $this->childId;
    }

    /**
     * @param array $childId
     */
    public function setChildId(array $childId): void
    {
        $this->childId = $childId;
    }


    public function getChoices(): ?array
    {
        return $this->choices;
    }

    public function setChoices(?array $choices): self
    {
        $this->choices = $choices;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    public function setMovie(?Movie $movie): self
    {
        $this->movie = $movie;

        return $this;
    }

    public function getParent(): ?bool
    {
        return $this->parent;
    }

    public function setParent(?bool $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
}
